<?php
namespace {
    define('APP_PATH', dirname(dirname(__FILE__)).'/');
    define('LOG_PATH', APP_PATH.'log/');
    define('DOCROOT_PATH', APP_PATH.'www/');
    define('APP_ID', 'app_id_used_for_session_id');
    define('SITE_NAME', 'screen_site_name');
    define('FILE_REPOSITORY_PATH', APP_PATH.'files/');

    if (isset($_SERVER['HTTP_HOST'])) {
        define(
            'SITE_URL',
            (empty($_SERVER['HTTPS']) ? 'http' : 'https').
                '://'.$_SERVER['HTTP_HOST'].'/'
        );
    }
}

namespace xa {
    /**
     * @todo move all constants here
     */
    abstract class Config {
        public static $db;
    }



    Config::$db = [
        'type' => 'pgsql',
        'host' => '127.0.0.1',
        'name' => 'db_name',
        'user' => 'user_name',
        'password' => 'password'
    ];
}
