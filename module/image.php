<?php
require '../config/app.php';
require APP_PATH.'lib/xa/xa.php';

use \xa\In;


if (
    !In::is_int($_GET['a'])
    ||
    !In::is_int($_GET['b'])
) {
	http_response_code(404);
	die('no file_id');
}

$file_id = intval($_GET['a'].$_GET['b']);
$photo = \xa::table('photo')->find($file_id);

if (!$photo) {
	http_response_code(404);
	die('no photo');
}

if (!In::in_dict($_GET['size'], ['thumb', 'logo', 'biglogo'])) {
    $_GET['size'] = 'photo';
}

$size = $_GET['size'];

$image_path = \DOCROOT_PATH.$size.'/'.
    \xa\File::sub($file_id).'-'.$photo['slug'].'.jpg';

if (!is_writable(dirname($image_path))) {
	if (!mkdir(dirname($image_path), 0777, true)) {
		throw new \Exception(
            'Could not create directory '.dirname($image_path),
            E_USER_ERROR
        );
	}
}

$image = new \xa\Image();
$image->open(\xa\File::path($file_id));

$quality = 80;

if ($size === 'thumb') {
	$image->fill(180, 180);
} elseif ($size === 'logo') {
	$image->fit(200, 200);
} elseif ($size === 'biglogo') {
	$quality = 90;
	$image->fit(400, 400);
}

$image->output($image_path, $quality);
chmod($image_path, 0666);

header('Content-Type: image/jpeg');
readfile($image_path);
