<?php
require '../config/app.php';
require APP_PATH.'lib/xa/xa.php';

define('MODULE_PATH', APP_PATH.'module/front/');

$benchmark = new \xa\tool\Benchmark();
$main_benchmark = $benchmark->start();

if (isset($_REQUEST[session_name()])) {
	$_COOKIE[session_name()] = $_REQUEST[session_name()];
}

session_start();

$user = new \xa\User();

$action = \xa\in::symbol($_GET['action']) ?: 'home';
$action_file_path = MODULE_PATH.'actions/'.$action.'.php';

if (file_exists($action_file_path)) {
	include $action_file_path;
} else {
	include MODULE_PATH.'actions/404.php';
}

header('ExecutionDuration: '.$benchmark->getTime($main_benchmark));
